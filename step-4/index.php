<?php include("../templates/header.php"); ?>

<h2>It doesn't have to be a form though</h2>
<b>You can load in content by clicking a link with the same principal:</b>
<ul>
    <li>Stop click event.</li>
    <li>Do ajax request.</li>
    <li>Win at life.</li>
</ul>
<br />

<a href="/step-4/thanks.php">Click here to see what I mean</a>

<div id="container" style="background: #ccc; margin: 30px 0; padding: 30px 0;">
    This container will have the content replaced with ajax with what thanks.php gives us back.<br />
    <b>Note</b> we have stripped out the header/footer.
</div>




<script>
// This wrapper makes sure the jQuery code only runs AFTER jquery is loaded
$(document).ready(function() {
    
    $("a").click(function(event) {
        
        // Stop the link from going to its href
        event.preventDefault();
        
        var theLink = $(this);
        
        // Don't need to use post as we're not sending anything
        $.get(theLink.attr("href"), function(response) {
            $("#container").html(response);
        });
        
    });
    
});
</script>

<?php include("../templates/footer.php"); ?>