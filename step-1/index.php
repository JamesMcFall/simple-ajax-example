<?php include("../templates/header.php"); ?>

<h2>A normal form with no ajax</h2>

<form action="/step-1/thanks.php" method="post" id="myform">
    
    <!-- Your Name -->
    <label for="your_name">Your Name</label><br />
    <input type="text" name="your_name" />
    <br /><br />
    
    <!-- Your Email -->
    <label for="your_email">Your Email</label><br />
    <input type="text" name="your_email" />
    <br /><br />
    
    <!-- Your Mesage -->
    <label for="your_message">Your Message</label><br />
    <textarea name="your_message"></textarea>
    <br /><br />
    
    <input type="submit" />
    
</form>

<?php include("../templates/footer.php"); ?>