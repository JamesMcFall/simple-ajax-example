<?php include("../templates/header.php"); ?>

<h2>Ajax form - posts to thanks.php and shows us the content</h2>

<div id="container" style="background: #ccc; margin: 30px 0; padding: 30px 0;">
    This container will have the content replaced with ajax with what thanks.php gives us back.<br />
    <b>Note</b> we have stripped out the header/footer.
</div>

<form action="/step-3/thanks.php" method="post" id="myform">
    
    <!-- Your Name -->
    <label for="your_name">Your Name</label><br />
    <input type="text" name="your_name" />
    <br /><br />
    
    <!-- Your Email -->
    <label for="your_email">Your Email</label><br />
    <input type="text" name="your_email" />
    <br /><br />
    
    <!-- Your Mesage -->
    <label for="your_message">Your Message</label><br />
    <textarea name="your_message"></textarea>
    <br /><br />
    
    <input type="submit" />
    
</form>



<script>
// This wrapper makes sure the jQuery code only runs AFTER jquery is loaded
$(document).ready(function() {
    
    $("form#myform").submit(function(event) {
        
        // Stop the form submitting
        event.preventDefault();
        
        var theForm = $(this);
        
        // Convert the form data into an array
        var formData = theForm.serializeArray();
        
        $.post(theForm.attr("action"), formData, function(response) {

            console.log(response)
            $("#container").html(response);

        });
        
        
        
    });
    
});
</script>

<?php include("../templates/footer.php"); ?>