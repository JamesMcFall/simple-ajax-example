<?php include("../templates/header.php"); ?>

<h2>A normal form - but jQuery catches the submit event</h2>

<form action="/step-2/thanks.php" method="post" id="myform">
    
    <!-- Your Name -->
    <label for="your_name">Your Name</label><br />
    <input type="text" name="your_name" />
    <br /><br />
    
    <!-- Your Email -->
    <label for="your_email">Your Email</label><br />
    <input type="text" name="your_email" />
    <br /><br />
    
    <!-- Your Mesage -->
    <label for="your_message">Your Message</label><br />
    <textarea name="your_message"></textarea>
    <br /><br />
    
    <input type="submit" />
    
</form>

<script>
// This wrapper makes sure the jQuery code only runs AFTER jquery is loaded
$(document).ready(function() {
    
    $("form#myform").submit(function(event) {
        
        // Stop the form submitting
        event.preventDefault();
        
        alert("We stopped the form from being able to submit, so no page refresh!");
        
        
    });
    
});
</script>

<?php include("../templates/footer.php"); ?>